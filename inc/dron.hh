#ifndef DRON_HH
#define DRON_HH

#include "Wektor3D.hh"
#include "lacze_do_gnuplota.hh"
#include "bryla.hh"
#include "zakres.hh"
#include "MacierzRot3D.hh"
#include "przeszkoda.hh"
#include <vector>
using namespace std;

/** @brief Klasa definiująca Drona składającego się z kilku brył, dziedziczy po klasie Prostopadloscian */
template<int rozmiar1, int rozmiar2>
class Dron: public Prostopadloscian<rozmiar1>{
    Prostopadloscian<rozmiar1> korpus;
    Prostopadloscian<rozmiar2> wirnikL,wirnikP;
    Zakres zakres;

    public:
    Prostopadloscian<rozmiar1> wezkorpus() const { return korpus; }
    Prostopadloscian<rozmiar1> &wezkorpus() { return korpus; }
    Prostopadloscian<rozmiar2> wezwirnikL() const { return wirnikL; }
    Prostopadloscian<rozmiar2> &wezwirnikL() { return wirnikL; }
    Prostopadloscian<rozmiar2> wezwirnikP() const { return wirnikP; }
    Prostopadloscian<rozmiar2> &wezwirnikP() { return wirnikP; }
    Zakres wezzakres() const { return zakres;}
    Zakres &wezzakres() {return zakres;} 

    /** @brief konstruktor klasy dron, nadaje mu konkretne położenie i ustawia jego początkowe położenie */
    Dron(){
        Wektor3D wspol;
        wspol[0]=0;
        wspol[1]=0;
        wspol[2]=0;
        for(int i=0;i<rozmiar1;i++){
            for(int j=0;j<3;j++){
                korpus.wezprostopadloscian().push_back(wspol);
            }
        }
        for(int k=0;k<rozmiar2;k++){
            for(int l=0;l<3;l++){
                wirnikL.wezprostopadloscian().push_back(wspol);
                wirnikP.wezprostopadloscian().push_back(wspol);
            }
        }
        korpus.wezkat()=0;
        wirnikP.wezkat()=0;
        wirnikL.wezkat()=0;
    }
    /** @brief metoda pozwalająca stworzyć zakres, jaki posiada bryła/klasa*/
    Zakres stworzzakres(){
        zakres.wezxmin()=korpus.minimalne('X');
        zakres.wezxmax()=korpus.maksymalne('X');
        zakres.wezymin()=korpus.minimalne('Y');
        zakres.wezymax()=korpus.maksymalne('Y');
        zakres.wezzmin()=korpus.minimalne('Z');
        zakres.wezzmax()=korpus.maksymalne('Z');
        return zakres;  
    }
    
};

#endif