#ifndef ZAKR_HH
#define ZAKR_HH

#include <iostream>
#include <iomanip>
#include "bryla.hh"
#include "Wektor3D.hh"
#include "lacze_do_gnuplota.hh"
using namespace std;

/** @brief Klasa przetrzymująca dane zakresów konkretynch brył*/
class Zakres{
    double xmin,xmax;
    double ymin,ymax;
    double zmin,zmax;

    public:
    double wezxmin() const { return xmin; }
    double &wezxmin() { return xmin; }
    double wezxmax() const { return xmax; }
    double &wezxmax() { return xmax; }
    double wezymin() const { return ymin; }
    double &wezymin() { return ymin; }
    double wezymax() const { return ymax; }
    double &wezymax() { return ymax; }
    double wezzmin() const { return zmin; }
    double &wezzmin() { return zmin; }
    double wezzmax() const { return zmax; }
    double &wezzmax() { return zmax; }

/** @brief konstruktor klasy zakres zerujący wartości danych*/
    Zakres()
    {
        double xmin = 0;
        double xmax = 0;
        double ymin = 0;
        double ymax = 0;
        double zmin = 0;
        double zmax = 0;
    }
};
#endif