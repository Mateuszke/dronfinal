#ifndef SCENA_HH
#define SCENA_HH

#include "Wektor3D.hh"
#include "lacze_do_gnuplota.hh"
#include "bryla.hh"
#include "dron.hh"
#include "powierzchnia.hh"
#include <vector>
using namespace std;

/** @brief klasa scena zawiera w sobie wszystkie wyswieltane elementy, dziedziczy po klasie Prostopadloscian**/
template<int rdron,int rwirnik,int rwoda, int rziemia>
class Scena: public Prostopadloscian<rdron||rwirnik||rwoda||rziemia>{
    Dron<rdron,rwirnik> dron;
    Powierzchnia<rwoda> woda;
    Powierzchnia<rziemia> ziemia;
    vector<Przeszkoda <rdron>> przeszkody;
    PzG::LaczeDoGNUPlota Lacze;
    public:
    /** @brief metody pozwalające na dostęp do pól prywatnych**/
    Dron<rdron,rwirnik> wezdrona() const{return dron;}
    Dron<rdron, rwirnik> &wezdrona() { return dron; }
    Powierzchnia<rwoda> wezwode() const { return woda; }
    Powierzchnia<rwoda> &wezwode() { return woda; }
    Powierzchnia<rziemia> wezziemie() const { return ziemia; }
    Powierzchnia<rziemia> &wezziemie() { return ziemia; }
    vector<Przeszkoda<rdron>> wezprzeszkody() const { return przeszkody; }
    vector<Przeszkoda<rdron>> &wezprzeszkody() { return przeszkody; }
    Przeszkoda<rdron> wezprzeszkode(int ind){return przeszkody[ind];}
    
    /** @brief konstruktor sceny**/
    Scena(){
        Lacze.ZmienTrybRys(PzG::TR_3D);
        Lacze.UstawZakresX(-40, 100);
        Lacze.UstawZakresY(-100, 100);
        Lacze.UstawZakresZ(-100, 100);
        Lacze.UstawRotacjeXZ(80, 330);
        Lacze.UsunWszystkieNazwyPlikow();
        Lacze.DodajNazwePliku("bryly/wirnikL.dat");
        Lacze.DodajNazwePliku("bryly/wirnikP.dat");
        Lacze.DodajNazwePliku("bryly/ziemia.dat");
        Lacze.DodajNazwePliku("bryly/pow_wody.dat");
        Lacze.DodajNazwePliku("bryly/drone.dat");
        Lacze.DodajNazwePliku("bryly/przeszkoda1.dat");
        Lacze.DodajNazwePliku("bryly/przeszkoda2.dat");
        Lacze.DodajNazwePliku("bryly/przeszkoda3.dat");
    }
    
    void rysuj(){
        Lacze.Rysuj();
    }

    /*** @brief funkcja porównójąca zakresy dwóch brył, pozwala na detekcję kolicji*/
    int porownajzakresy(Zakres zak1,Zakres zak2){                        // https://eli.thegreenplace.net/2008/08/15/intersection-of-1d-segments?fbclid=IwAR2ZrEpqlj3V9TosEa58SFX7o-ZcRVUBmfVTWQH-0jgbYlRi89sqdg9HXZs
    
       int a=0;                                                            //Zapewne tu jest jakiś błąd...
        if (((zak1.wezxmin() <= zak2.wezxmin()) &&
            (zak1.wezxmax() >= zak2.wezxmin())) ||
            ((zak1.wezxmin() <= zak2.wezxmax()) &&
             (zak1.wezxmax() >= zak2.wezxmax())))
        {
            a++;//cout<<"1";
        }

        if (((zak1.wezymin() <= zak2.wezymin()) &&
             (zak1.wezymax() >= zak2.wezymin())) ||
            ((zak1.wezymin() <= zak2.wezymax()) &&
             (zak1.wezymax() >= zak2.wezymax())))
        {
            a++;//cout<<"2";
        }
        if (((zak1.wezzmin() <= zak2.wezzmin()) &&
             (zak1.wezzmax() >= zak2.wezzmin())) ||
            ((zak1.wezzmin() <= zak2.wezzmax()) &&
             (zak1.wezzmax() >= zak2.wezzmax())))
        {
            a++;//cout<<"3";
        }
        cout<<endl;
        if (a == 3)
            return 1;
        else
        return 0; 
    }

/** @brief funkcja pozwalająca na sprawdzenie czy nastąpiła kolicja dwóch brył, w zależności od typu elementów zwraca różne wartości */
    double czykolizja(){
        double kolizja=0;
        kolizja=porownajzakresy(ziemia.wezzakres(),dron.wezzakres());
        if(kolizja==1){
            return 1;
        }
        kolizja=porownajzakresy(woda.wezzakres(),dron.wezzakres());
        if(kolizja==1){
            return 2;
        }
        for(unsigned int i=0;i<przeszkody.size();i++){
            kolizja=porownajzakresy(przeszkody[i].wezzakres(),dron.wezzakres());
            if(kolizja==1){
                return 3;
            }
        }
        return 0;
    }


};

#endif