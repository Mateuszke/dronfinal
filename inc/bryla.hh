#ifndef PROST_HH
#define PROST_HH

#include "Wektor3D.hh"
#include "MacierzRot3D.hh"
#include "lacze_do_gnuplota.hh"
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
using namespace std;


/** @brief Klasa modelująca trojwymiarową bryłę 
 * @param points dynamiczna tablica przehowujaca informacje o polozeniu bryly*
 * @param translacja trójwymiarowy wektor przesuniecia*
 * @param orientacja wektor pozwalający na ustalenie kierunku w danym momencie*/
template <int linie>
class Prostopadloscian{
    std::vector<Wektor3D> points;
    Wektor3D translacja;
    Wektor3D orientacja;
    MacierzRot3D rotacja;
    double kat;

public:
    Wektor3D operator[](int Ind) const { return points[Ind]; }
    Wektor3D &operator[](int Ind) { return points[Ind]; }
    vector<Wektor3D> wezprostopadloscian() const { return points; }
    vector<Wektor3D> &wezprostopadloscian() { return points; }
    Wektor3D wezprzesuniecie() const { return translacja; }
    Wektor3D &wezprzesuniecie() { return translacja; }
    Wektor3D wezorientacja() const { return orientacja; }
    Wektor3D &wezorientacja() { return orientacja; }
    double wezkat() const { return kat; }
    double &wezkat() { return kat; }



/***
 * @brief konstruuje nowy Prostopadloscian:
 * 
 */
    Prostopadloscian(){
        Wektor3D wspol;
        wspol[0]=0;wspol[1]=0;wspol[2]=0;
        for(int i=0;i<linie;i++){
            for(int j=0;j<3;j++){
                points.push_back(wspol);
            }
        }
    } 
            /*** @brief Metoda pobierająca współżędne punktów z pliku o danej nazwie **/ 
    void WezZPliku(string nazwa){
        ifstream plik;
        plik.open(nazwa.c_str());
        for(int i=0;i<linie;i++){
            for(int j=0;j<3;j++){
                plik>>points[i][j];
                cout<<points[i][j]<<" to biore z pliku\n"<<endl;
            }
        }
    }
            /*** @brief Metoda zapisująca współżędne do pliku **/
    void DajDoPliku(string gdzie){
        ofstream zapis(gdzie);
        for(int i=0;i<linie;i++){
            for(int j=0;j<3;j++){
                zapis<<points[i][j]<<" ";
                cout<<points[i][j]<<" to daje do pliku\n"<<endl;
            }
            zapis<<endl;
        }
        zapis.close();
    }

           
/** @brief rysuje prostopadłościan do pliku**/
    void rysuj(std::string filename){
        ofstream outputFile;
        outputFile.open(filename);
        for(int i=0;i<linie;++i){
            outputFile << points[i] + translacja << endl;
            if(i % 4 == 3) {
                outputFile << "#\n\n";
            }
        }
    }

    /** @brief metoda pozwalająca na określenie ruchu, jakim będzie poruszała się bryłą wzdłóż danych osi */
    Wektor3D ruch(double poY, double poZ)
    {
        Wektor3D wek;
        Wektor3D osZ, osY;     
        osZ[0] = 0;
        osZ[1] = 0;
        osZ[2] = 1;
        
        osY = orientacja;               
        osZ = osZ * poZ;       
        osY = orientacja * poY;
        wek= osZ + osY; 
        return wek;
    }

    /**
     * @brief przesuwa prostopadłościan o wektor
     * 
     * @param change wektor przesunięcia [x,y,z]
     */
    void translate(const Wektor3D& change)
    {
        translacja = translacja + change;
    }

    /**
     * @brief obraca prostopadłościan według osi Z
     * 
     * @param kat kąt obrotu w stopniach
     */
    void rotateZ(int kat)
    {
        MacierzRot3D mac;
        mac.UstawRotZ_st(kat);
        rotacja = mac;
        for (unsigned i = 0; i < points.size(); ++i)
        {
          points[i] = rotacja * points[i];
        }
        orientuj();
    }

    void orientuj(){
        orientacja=points[1]-points[0];
        orientacja=orientacja/10;
        orientacja[2]=0;
    }

    double minimalne(char pom){
        double a=100;
        if(pom=='X'){
            for(int i=0;i<linie;i++){
                a=min(points[i][0],a);
            }
        }
        if(pom=='Y'){
            for(int j=0;j<linie;j++){
                a=min(points[j][1],a);
            }
        }
        if(pom=='Z'){
            for(int k=0;k<linie;k++){
                a=min(points[k][2],a);
            }
        }
        return a;
    }

    double maksymalne(char pom){
        double a=-100;
        if(pom=='X'){
            for(int i=0;i<linie;i++){
                a=max(points[i][0],a);
            }
        }
        if(pom=='Y'){
            for(int j=0;j<linie;j++){
                a=max(points[j][1],a);
            }
        }
        if(pom=='Z'){
            for(int k=0;k<linie;k++){
                a=max(points[k][2],a);
            }
        }
        return a;
    }
    

};  
#endif
