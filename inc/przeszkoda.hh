#ifndef PRZESZKODA_HH
#define PRZESZKODA_HH
#include <iostream>
#include <iomanip>
#include "bryla.hh"
#include "Wektor3D.hh"
#include "zakres.hh"
#include "lacze_do_gnuplota.hh"
using namespace std;

/** @brief Klasa definiująca przeszkodę jako osobny element sceny, dziedziczy po klasie Prostopadloscian */
template<int rozmiar>
class Przeszkoda: public Prostopadloscian<rozmiar>{
    Prostopadloscian<rozmiar> przeszkoda;
    Zakres zakres;
    public:
    Prostopadloscian <rozmiar> wezprzeszkode() const{return przeszkoda;}
    Prostopadloscian<rozmiar> &wezprzeszkode() { return przeszkoda; }
    Zakres wezzakres() const { return zakres;}
    Zakres &wezzakres() {return zakres;} 
    

    Przeszkoda(){
        Wektor3D wspol;
        wspol[0] = 0;
        wspol[1] = 0;
        wspol[2] = 0;
        for(int i=0;i<rozmiar;i++){
            for(int j=0;j<3;j++){
                przeszkoda.wezprostopadloscian().push_back(wspol);
            }
        }
        /*for(int k=0;k<4;k++){
            wektororientacji.push_back(wspol)
        }*/
    }   

    Zakres stworzzakres(){
        
        zakres.wezxmin()=przeszkoda.minimalne('X');
        zakres.wezxmax()=przeszkoda.maksymalne('X');
        zakres.wezymin()=przeszkoda.minimalne('Y');
        zakres.wezymax()=przeszkoda.maksymalne('Y');
        zakres.wezzmin()=przeszkoda.minimalne('Z');
        zakres.wezzmax()=przeszkoda.maksymalne('Z');
        return zakres;
    } 
};
#endif