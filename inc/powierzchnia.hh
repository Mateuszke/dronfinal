#ifndef POWIERZCHNIA_HH
#define POWIERZCHNIA_HH

#include <iostream>
#include <iomanip>
#include "bryla.hh"
#include "przeszkoda.hh"
#include "Wektor3D.hh"
#include "lacze_do_gnuplota.hh"

using namespace std;

/** @brief Klasa stworzona w celu odosobnienia nieruchomych powierzchni wody i dna od innych ruchomych elementów sceny, dziedziczy po klasie prostopadloscian*/
template <int rozmiar>
class Powierzchnia:public Prostopadloscian<rozmiar>{
    Prostopadloscian<rozmiar> powierzchnia;
    Zakres zakres;
    public:
    Prostopadloscian<rozmiar> wezpowierzchnie() const {return powierzchnia;}
    Prostopadloscian<rozmiar> &wezpowierzchnie() {return powierzchnia;}
    Zakres wezzakres()const {return zakres;}
    Zakres &wezzakres() {return zakres;}

    Powierzchnia():Prostopadloscian<rozmiar>(){};

    Zakres stworzzakres(){
        zakres.wezxmin()=powierzchnia.minimalne('X');
        zakres.wezxmax() = powierzchnia.maksymalne('X');
        zakres.wezymin() = powierzchnia.minimalne('Y');
        zakres.wezymax() = powierzchnia.maksymalne('Y');
        zakres.wezzmin() = powierzchnia.minimalne('Z');
        zakres.wezzmax() = powierzchnia.maksymalne('Z');
        return zakres;  
    }
};


#endif