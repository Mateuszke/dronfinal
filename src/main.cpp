#include <iostream>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string>
#include <cassert>
#include "lacze_do_gnuplota.hh"
#include "Wektor3D.hh"
#include "MacierzRot3D.hh"
#include "bryla.hh"
#include "dron.hh"
#include "scena.hh"
#include "powierzchnia.hh"
#include <chrono>
#include <thread>
#include <cmath>

using namespace std;
using namespace std::this_thread;
using namespace std::chrono;


int main(){
    
  
  PzG::LaczeDoGNUPlota Lacze;
  Dron<20,34> dron=Dron<20,34>();//wymiar drona,wirnik
  Scena<20,28,89,25> scena=Scena<20,28,89,25>();//wymiar dron,wirnik,woda,ziemia
  Przeszkoda<20> przeszkoda=Przeszkoda<20>();
  Wektor3D przesuniecie;
  Lacze.Inicjalizuj();
  
  scena.wezziemie().WezZPliku("bryly/ziemia.dat");
  scena.wezziemie().wezpowierzchnie().WezZPliku("bryly/ziemia.dat");
  scena.wezwode().wezpowierzchnie().WezZPliku("bryly/pow_wody.dat");
  
  scena.wezdrona().wezkorpus().WezZPliku("bryly/model2.dat");
  scena.wezdrona().wezkorpus().DajDoPliku("bryly/drone.dat");
  scena.wezdrona().wezkorpus().rysuj("bryly/drone.dat");
  scena.wezdrona().wezwirnikL().WezZPliku("bryly/wirnikLpierw.dat");
  scena.wezdrona().wezwirnikL().DajDoPliku("bryly/wirnikL.dat");
  scena.wezdrona().wezwirnikL().rysuj("bryly/wirnikL.dat");
  scena.wezdrona().wezwirnikP().WezZPliku("bryly/wirnikPpierw.dat");
  scena.wezdrona().wezwirnikP().DajDoPliku("bryly/wirnikP.dat");
  scena.wezdrona().wezwirnikP().rysuj("bryly/wirnikP.dat");
  przeszkoda.WezZPliku("bryly/przeszkoda1");
  scena.wezprzeszkody().push_back(przeszkoda);
  przeszkoda.WezZPliku("bryly/przeszkoda2.dat");
  scena.wezprzeszkody().push_back(przeszkoda);
  przeszkoda.WezZPliku("bryly/przeszkoda3.dat");
  scena.wezprzeszkody().push_back(przeszkoda);
  scena.wezprzeszkode(0).wezzakres().wezxmax();
  scena.wezdrona().wezkorpus().orientuj();
  scena.wezdrona().wezwirnikL().orientuj();
  scena.wezdrona().wezwirnikP().orientuj();
  
  scena.rysuj();
  

  cout << endl
       << endl
       << "#####################################\n";
  cout << "#               MENU:               #\n";
  cout << "#   r - Zadaj ruch na wprost        #\n"; /*przemieszcza dronea o zadaną odległość, wznosząc go lub obniżając*/
  cout << "#   o - Zadaj zmianę orientacji     #\n"; /*obraca dronea według osi Z*/
  cout << "#   m - wyświetl menu               #\n"; /*wyswietla ponownie menu opcji*/
  cout << "#                                   #\n";
  cout << "#   k - Zakoncz działanie programu  #\n"; /*koniec dzialania programu*/
  cout << "#####################################\n";

  char wybor;               //wybor w menu         
  double a;                //kąt obrotu
  double b;                //kąt opadania/wznoszenia
  double odlegloscZ;       //odległość opadania/wznoszenia (zmienna z - wertykalna)
  double odlegloscY;
  double odlegloscR;       //odległość przesunięcia 

  do
  {
      cout << "Twoj wybor (m - menu):  ";
      cin >> wybor;
      switch (wybor)
      {
        case 'r':
        {
            cout << endl;
            cout << "Podaj wartość kąta (wznoszenia/opadania) w stopniach" << endl;
            cout << "Wartość kąta:   ";
            cin >> b;
            scena.wezdrona().wezkat()=scena.wezdrona().wezkat()+b;
            cout << endl;
            cout << "Podaj wartość odległości, na którą ma się przemiescić dron" << endl;
            cout << "Wartość odległości:   ";
            cin >> odlegloscR;
            cout << endl;
            
            odlegloscZ = odlegloscR * sin((b*M_PI)/180); //na radiany
            odlegloscY = odlegloscR * cos((b*M_PI)/180);
            Wektor3D ruch;
            ruch=scena.wezdrona().wezkorpus().ruch(odlegloscY,odlegloscZ);
            for(int i=0;i<50;i++){
                scena.wezdrona().wezkorpus().translate(ruch/50);
                scena.wezdrona().wezwirnikL().translate(ruch/50);
                scena.wezdrona().wezwirnikP().translate(ruch/50);
                scena.wezdrona().wezkorpus().rysuj("bryly/drone.dat");
                scena.wezdrona().wezwirnikL().rysuj("bryly/wirnikL.dat");
                scena.wezdrona().wezwirnikP().rysuj("bryly/wirnikP.dat");
                scena.rysuj();
                //cout<<scena.wezdrona().wezzakres().wezxmax()<<endl;
                //cout<<scena.wezziemie().wezzakres().wezxmax()<<endl;
                //cout<<scena.wezprzeszkode(0).wezzakres().wezxmax();
                int blad;
                cout<<scena.czykolizja()<<endl;
                blad=scena.czykolizja();
                cout<<blad<<endl;
                if(blad>0){
                    cout<<"kolizja\n";
                    
                }
                chrono::milliseconds timespan(10);
                this_thread::sleep_for(timespan);
            }
            break;
        }
            
        case 'o':
        {
          cout << "Podaj wartość kąta obrotu w stopniach" << endl;
          cout << "Wartość kąta:  ";
          cin >> a;
          for (int i = 0; i < 20; i++){
              scena.wezdrona().wezkorpus().rotateZ(a/30);
              scena.wezdrona().wezwirnikL().rotateZ(a / 30);
              scena.wezdrona().wezwirnikP().rotateZ(a / 30);
              scena.wezdrona().wezkorpus().rysuj("bryly/drone.dat");
              scena.wezdrona().wezwirnikL().rysuj("bryly/wirnikL.dat");
              scena.wezdrona().wezwirnikP().rysuj("bryly/wirnikP.dat");
              scena.rysuj();
              chrono::milliseconds timespan(50);
              this_thread::sleep_for(timespan);
          }
        break;
        }
        case 'm':
        {
          cout << endl
               << endl
               << "#####################################\n";
          cout << "#               MENU:               #\n";
          cout << "#   r - Zadaj ruch na wprost        #\n";
          cout << "#   o - Zadaj zmianę orientacji     #\n";
          cout << "#   m - wyświetl menu               #\n";
          cout << "#                                   #\n";
          cout << "#   k - Zakoncz działanie programu  #\n";
          cout << "#####################################\n";
          break;
        }
        case 'k':
        {
          cout << "Koniec działania programu" << endl;
          exit(0);
          break;
        }
        default:
        {
          cout << "Nie ma takiej opcji. Prosze sprobowac ponownie" << endl
               << endl;
          break;
        }
    }
  } while (true);
}
